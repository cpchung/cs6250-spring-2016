#!/usr/bin/python

"Test client - This opens a connection to the IP and port specified by \
    the user in the command line. Sends over what is typed into the client."

# Based on http://pymotw.com/2/socket/tcp.html

import socket
import sys

#if not (len(sys.argv) == 3):
#    print "Syntax:"
#    print "    python test-client.py <server-ip> <port>"
#    print "  server-ip is the IP address running the server."
#    print "  port is the TCP port that the server is running."
#    exit()

def runtest(address, port):
    "Runs a test to try to connect to the given address and port"
    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(5.0) 
    amount_received = 0
    connected = 0

    try:
        # Connect the socket to the port where the server is listening
        server_address = (address, int(port))
        print >>sys.stderr, '\tconnecting to %s port %s' % server_address
        sock.connect(server_address)
        connected = 1
    
    
        # Send data
        message = 'This is the message.  It will be repeated.'
        #print >>sys.stderr, 'sending "%s"' % message
        sock.sendall(message)

        # Look for the response
        amount_expected = len(message)
    
        while amount_received < amount_expected:
            data = sock.recv(16)
            amount_received += len(data)
            #print >>sys.stderr, 'received "%s"' % data
    #except Exception as e:
    #    print e
    except:
        if connected:
            #print >>sys.stderr, 'closing socket'
            sock.close()
        
    if amount_received > 0:
        return 1
    else:
        return 0


print '***** Starting tests for W1'

# Should not connect to any w node on port 1080
if runtest('10.0.0.1', 1080):
    print 'FAIL: was able to connect to e1'
else:
    print 'PASS: was not able to connect to e1'
    
if runtest('10.0.0.2', 1080):
    print 'FAIL: was able to connect to e2'
else:
    print 'PASS: was not able to connect to e2'
    
if runtest('10.0.0.3', 1080):
    print 'FAIL: was able to connect to e3'
else:
    print 'PASS: was not able to connect to e3'
    
if runtest('10.0.0.1', 1090):
    print 'FAIL: was able to connect to e1'
else:
    print 'PASS: was not able to connect to e1'
    
if runtest('10.0.0.2', 1090):
    print 'PASS: was able to connect to e2'
else:
    print 'FAIL: was not able to connect to e2'
    
if runtest('10.0.0.3', 1090):
    print 'PASS: was able to connect to e3'
else:
    print 'FAIL: was not able to connect to e3'
    
