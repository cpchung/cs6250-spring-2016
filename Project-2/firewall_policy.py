#!/usr/bin/python
															
"Project 2 - This creates the firewall policy. "

from pyretic.lib.corelib import *
from pyretic.lib.std import *
from pyretic.lib.query import packets
from pyretic.core import packet


# Rule number, srcmac, dstmac, srcip, dstip, srcport, dstport
# HWaddr 00:00:00:00:00:01
# inet addr:10.0.0.1 

def policy_woStar(entry):

    print entry['rulenum']

    rule=match()
    if entry['srcmac'] != '*':
        print  "entry['srcmac']:", entry['srcmac']
        rule = rule & match(srcmac=MAC(entry['srcmac']))

    if entry['dstmac'] != '*':
        print "entry['dstmac']: ", entry['dstmac']
        rule = rule & match(dstmac=MAC(entry['dstmac']))

    if entry['srcip'] != '*':
        print "entry['srcip']: ", entry['srcip']
        rule = rule & match(srcip=entry['srcip'])

    if entry['dstip'] != '*':
        print "entry['dstip']: ", entry['dstip']
        rule = rule & match(dstip=entry['dstip'])

    if entry['srcport']!='*':
        print "entry['srcport']: ", entry['srcport']
        rule = rule & match(srcport=int(entry['srcport']), ethtype=packet.IPV4, protocol=packet.TCP_PROTO)

    if entry['dstport']!='*':
        print "entry['dstport']: ", entry['dstport']
        rule = rule & match(dstport=int(entry['dstport']), ethtype=packet.IPV4, protocol=packet.TCP_PROTO)

    return rule    


def make_firewall_policy(config):
    # TODO - This is where you need to write the functionality to create the
    # firewall. What is passed in is a list of rules that you must implement.
    
    # feel free to remove the following "print config" line once you no longer need it
    print config # for demonstration purposes only, so you can see the format of the configl

    print '*********************************'
    rules = []
    for entry in config:
        # TODO - build the individual rules 

        # examples: 

        # print entry['dstport'],entry['dstip']
        
        # rule=match()
        # print 'empty match pass'
        # rule = match(dstport=int(entry['dstport']), ethtype=packet.IPV4, protocol=packet.TCP_PROTO)
        # , dstip=entry['dstip']
        # rule=match(srcip=entry['srcip']) & match(dstport=int(entry['dstport']), ethtype=packet.IPV4, protocol=packet.TCP_PROTO)
        # rule=match(srcip=entry['srcip'],dstip=entry['dstip']) & match(dstport=int(entry['dstport']), ethtype=packet.IPV4, protocol=packet.TCP_PROTO)
        # rule =match(srcip=entry['srcip'])
        # rule =rule & match(dstip=entry['dstip'])
  
        # rule=match(dstport=1080,ethtype=packet.IPV4,protocol=packet.TCP_PROTO) 
        # rule = match(srcmac=MAC(entry['srcmac']))
        # rule = match(srcip=entry['srcip'])
        # rule = match(dstmac=MAC(entry['dstmac']), srcport=entry['srcport'])
        rules.append(policy_woStar(entry))
        # rules.append(rule)
        pass
    
    allowed = ~(union(rules))

    return allowed
