#!/usr/bin/python

import json
import Queue
import socket
import sys
import threading
import time
from pprint import pformat


# PORT_RANGE = range(1024, 65536)
PORT_RANGE = range(1024, 4001)

blocks = {}
cont = True

def tracker(queue):
    """Track the changing queue size"""
    global cont
    while (cont and queue.qsize() > 0):
        time.sleep(.5)
        sys.stdout.write('Queue size: {:0>3d} \r'.format(queue.qsize()))
        sys.stdout.flush()
    
def worker(queue):
    """Pull an (ip,port) tuple off the queue and attempt to connect"""
    global blocks, cont
    while (cont):
        try:
            addr = queue.get(False)
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.settimeout(.2)
            sock.connect(addr)
            sock.close()

        except Queue.Empty:
            time.sleep(1)
            continue

        except socket.error as e:
            if e.errno != 111:
                if addr[0] in blocks:
                    blocks[addr[0]].append(addr[1])
                else:
                    blocks[addr[0]] = [addr[1]]

def parseList(l):
    """Return the information a list conveys more concisely
    :param l: an unsorted list
    :returns: a list of ranges representing l
    """
    l_sorted = sorted(l)
    range_list = []
    for port in l_sorted:
        added = False
        if not range_list: 
            range_list.append([port, port])
            added = True
        else:
            for i in range(len(range_list)):
                if (range_list[i][1] == port - 1):
                    range_list[i][1] = port
                    added = True
        if not added:
            range_list.append([port, port])
        
    for i in range(len(range_list)):
        if range_list[i][0] == range_list[i][1]:
            range_list[i] = range_list[i][:1]

    return range_list

def main(my_ip):
    global cont
    addresses = [
        '10.0.0.1',
        '10.0.0.2',
        '10.0.0.3',
        '10.0.0.4',
        '10.0.0.5',
        '10.0.0.6',
    ]
    connection_queue = Queue.Queue(maxsize=100)

    # Create a list of threads to do the work
    threads = []
    for i in range(20):
        t = threading.Thread(target=worker, args=(connection_queue,))
        threads.append(t)
        t.start()
    update = threading.Thread(target=tracker, args=(connection_queue,))
    threads.append(update)
    update.start()

    # Now start to fill the queue so the threads have something to do
    addresses.remove(my_ip)
    for ip in addresses:
        for i in PORT_RANGE:
            # Add connection to the queue when a slot becomes available
            connection_queue.put((ip, i), True)

    while (connection_queue.qsize() > 0):
        continue

    # Stop the threads and wait for them to finish updating blocks
    cont = False 
    for t in threads:
        t.join()

    # Clarify the json output
    for k, v in blocks.iteritems():
        blocks[k] = parseList(v)

    print(pformat(blocks))

    outfile_name = '{}-blockages.json'.format(my_ip)
    with open(outfile_name, 'w') as outfile:
        json.dump(blocks, outfile)


if __name__ == '__main__':
    if len(sys.argv) != 2:
        sys.exit('Usage: $ python test_connection.py <src ip>\n')
    else: main(sys.argv[1]) 