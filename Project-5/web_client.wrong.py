'''
/==================================
 Author: Chak-Pong Chung
 Date created: 04/16/2016
 Summary: A simple web browser
==================================/ 
''' 

import socket
from urlparse import urlparse
import sys
import os

def head2Dict(s):
	s=s.split("\r\n")
	dic={}
	for i in range(1,len(s)):
		items=s[i].split(": ")
		if(len(items)>1):
			dic[items[0]]=items[1]
	return dic

# create a socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connect to the server
if len(sys.argv) != 3:
	raise ValueError('arguments not right!')

link = urlparse(sys.argv[1])
host=socket.gethostbyname(link.netloc)
s.connect((host, 80))
# s.connect((link.netloc,80))
## creating a correct HTTP GET request and printing it to stdout
# message = "GET "+link.path+" HTTP/1.1\r\n\
# Host:"+link.netloc+"\r\n\
# Connection: close\r\n\r\n" 

message = "GET "+sys.argv[1]+" HTTP/1.1\r\nHost:"+link.netloc+"\r\nConnection: close\r\n\r\n"
# message = "GET "+sys.argv[1]+" HTTP/1.1\r\nHost:"+host+"\r\nConnection: close\r\n\r\n"
# message = "GET "+link.path+" HTTP/1.1\r\nHost:"+link.netloc+"\r\nConnection: close\r\n\r\n"

print message
s.sendall(message)

## receive data
response = ''
try :
	data = s.recv(4096)
	if len(data) < 1:
		sys.stderr.write("recv() < 1")
		sys.exit()
	# else:
	# 	sys.stderr.write('first recv(4096)')
except socket.error:
	sys.stderr.write('recv() failed')
	sys.exit()

while data:
	response += data
	data = s.recv(4096)

# check whether the header exists in the response
if ("HTTP/1.1 " in response) and ("Connection: close" in response):
	# HTTP headers will be separated from the body by an empty line
	header, _, body = response.partition("\r\n\r\n")
	# printing the response header to stdout
	print header
	if ("200 OK" in response) and ("Content-Length" in header):
		head=head2Dict(header)
		content_length=int(head["Content-Length"])
		body=body[0:content_length]
		fhand = open(sys.argv[2],"wb")
		fhand.write(body)
		fhand.close()